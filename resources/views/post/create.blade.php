@extends('layouts.main')
@section('content')
<form method="POST" action="{{ route('post.store') }}">
  @csrf
  <div class="form-group mb-3">
    <label for="title">Title</label>
    <input type="text" value="{{ old('title') }}" name="title" class="form-control" id="title" placeholder="title">
    @error('title')
      <p class="text-danger">{{ $message }}</p>  
    @enderror
  </div>
  <div class="form-group mb-3">
    <label for="content">Content</label>
    <textarea type="text" name="content" title="content" class="form-control" id="content" placeholder="content">{{ old('content') }}</textarea>
    @error('content')
      <p class="text-danger">{{ $message }}</p>  
    @enderror
  </div>
  <div class="form-group mb-3">
    <label for="image">Image</label>
    <input type="text" value="{{ old('image') }}" name="image" class="form-control" id="image" placeholder="image">
    @error('image')
      <p class="text-danger">{{ $message }}</p>  
    @enderror
  </div>
  <div class="form-group mb-3">
    <label for="category">Category</label>
    <select class="form-select" id="category" name="category_id">
      @foreach($categories as $category)
        <option 
            {{ old('category_id') == $category->id ? 'selected' : ''}}

            value="{{ $category->id }}">{{ $category->title }}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group mb-3">
    <label for="tags">Tags</label>
    <select multiple class="form-select" id="tags" name="tags[]">
      @foreach($tags as $tag)
        <option 


            value="{{ $tag->id }}">{{ $tag->title }}</option>
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Create</button>
</form>
@endsection
