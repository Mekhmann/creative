<?php

namespace App\Components;

use GuzzleHttp\Client;

class importDataClient 
{
    public $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://jsonplaceholder.typicode.com/',
            'timeout' => 10.0,
            'verify' => false
        ]);
    }
}