<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Creative</title>
</head>
<body>
    <div class="container">
        
           <nav class="navbar navbar-expand-lg navbar-light bg-light">
               <div>
                 <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                   <span class="navbar-toggler-icon"></span>
                 </button>
                 <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                       <a class="nav-link active" aria-current="page" href="{{ route('main.index') }}">Home</a>
                     </li>
                     <li class="nav-item">
                      <a class="nav-link" href="{{ route('post.index') }}">Posts</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="{{ route('about.index') }}">About</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="{{ route('contacts.index') }}">Contacts</a>
                      </li>

                      @can('view', auth()->user())
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.post.index') }}">Admin</a>
                      </li>
                      @endcan
                   </ul>
                 </div>
               </div>
             </nav>
   
        @yield('content')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>